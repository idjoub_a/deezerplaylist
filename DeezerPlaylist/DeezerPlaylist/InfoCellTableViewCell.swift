//
//  InfoCellTableViewCell.swift
//  DeezerPlaylist
//
//  Created by Mac-maison on 04/02/2017.
//  Copyright © 2017 Mac-maison. All rights reserved.
//

import UIKit
import SnapKit

class InfoCellTableViewCell: UITableViewCell {

    var playlist_image = UIImageView()
    var playlist_title = UILabel()
    var playlist_date = UILabel()
    var playlist_length = UILabel()
    var playlist_user = UILabel()
    
    let bottom_line = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        components()
    }

    func components() {
        
        self.contentView.addSubview(bottom_line)
        bottom_line.backgroundColor = UIColor.black
        bottom_line.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(self.contentView)
            make.right.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView)
            make.height.equalTo(1)
        }
        
        self.contentView.addSubview(playlist_image)
        playlist_image.image = UIImage(named: "none")
        playlist_image.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(self.contentView).offset(20)
            make.centerY.equalTo(self.contentView)
            make.height.equalTo(80)
            make.width.equalTo(80)
        }
        
        self.contentView.addSubview(playlist_date)
        playlist_date.font = UIFont(name: "Arial-ItalicMT", size: 10)
        playlist_date.textColor = UIColor.black
        playlist_date.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(self.playlist_image.snp.right).offset(20)
            make.bottom.equalTo(self.contentView.snp.centerY).offset(-3)
        }
        
        self.contentView.addSubview(playlist_length)
        playlist_length.font = UIFont(name: "Arial", size: 18)
        playlist_length.textColor = UIColor.black
        playlist_length.snp.makeConstraints { (make) -> Void in
            make.left.equalTo((self.playlist_image.snp.right)).offset(20)
            make.top.equalTo(self.contentView.snp.centerY).offset(3)
        }
        
        self.contentView.addSubview(playlist_title)
        playlist_title.font = UIFont(name: "Arial-BoldMT", size: 18)
        playlist_title.textColor = UIColor.black
        playlist_title.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(self.playlist_image.snp.right).offset(20)
            make.bottom.equalTo(self.playlist_date.snp.top).offset(-7)
        }
        
        self.contentView.addSubview(playlist_user)
        playlist_user.font = UIFont(name: "Arial", size: 18)
        playlist_user.textColor = UIColor.black
        playlist_user.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(self.playlist_image.snp.right).offset(20)
            make.bottom.equalTo(self.contentView).offset(-7)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
