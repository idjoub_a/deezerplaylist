//
//  Playlist.swift
//  DeezerPlaylist
//
//  Created by Mac-maison on 05/02/2017.
//  Copyright © 2017 Mac-maison. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class Playlist {
    
    var title: String
    var picture_medium: String
    var nb_tracks: String
    var name: String
    var creation_date: String
    
    init(json: JSON) {
        self.title = json["title"].stringValue
        self.picture_medium = json["picture_medium"].stringValue
        self.nb_tracks = json["nb_tracks"].stringValue
        self.name = json["creator"]["name"].stringValue
        
        let dateString = json["creation_date"].stringValue
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-dd-MM hh:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        if let dateObj = dateFormatter.date(from: dateString) {
        
            dateFormatter.dateFormat = "MM-dd-yyyy"
            self.creation_date = dateFormatter.string(from: dateObj)
        }
        else {
            self.creation_date = ""
        }
    }
    
    init() {
        self.title = ""
        self.picture_medium = ""
        self.nb_tracks = ""
        self.name = ""
        self.creation_date = ""
    }
}
