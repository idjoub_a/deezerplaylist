//
//  NetworkManager.swift
//  DeezerPlaylist
//
//  Created by Mac-maison on 03/02/2017.
//  Copyright © 2017 Mac-maison. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import PKHUD

class NetworkHandler {

    //    func Request(method: HTTPMethod, parameters: [String : AnyObject], callback: @escaping (JSON) -> Void) {

    func Request(callback: @escaping (JSON) -> Void) {
        
        let url = "http://api.deezer.com/user/5/playlists"
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        Alamofire.request(url).response { response in
            //print("Request: \(response.request)")
            //            print("Response: \(response.response)")
            //            print("Error: \(response.error)")
            debugPrint(response)
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                //print("Data: \(utf8Text)")
                let json = JSON(data: data)
                if (json["error"] != JSON.null) {
                    SweetAlert().showAlert("Error", subTitle: String(describing: json["error"]["message"]), style: AlertStyle.error)
                }
                else if (json == JSON.null) {
                    SweetAlert().showAlert("Error", subTitle: "check internet connection", style: AlertStyle.error)
                }
                else {
                    callback(json)
                }
                PKHUD.sharedHUD.hide()
            }
        }
    }
}
