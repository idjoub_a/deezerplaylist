//
//  PlaylistViewController.swift
//  DeezerPlaylist
//
//  Created by Mac-maison on 03/02/2017.
//  Copyright © 2017 Mac-maison. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyJSON
import SDWebImage

class PlaylistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let list = UITableView()
    var list_playlists: [Playlist] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NetworkHandler().Request(callback: callback)
        componnent()
    }
    
    func callback(json: JSON) {
        if (json != nil) {
            for element in json["data"].arrayValue {
                list_playlists.append(Playlist(json: element))
            }
            list.reloadData()
        }
    }
    
    func componnent() {
        self.view.addSubview(list)
        list.backgroundColor = UIColor.clear
        list.separatorStyle = .none
        list.delegate = self
        list.dataSource = self
        list.register(InfoCellTableViewCell.self, forCellReuseIdentifier: "InfoCell")
        self.automaticallyAdjustsScrollViewInsets = false //no extra padding on top table view cell
        list.reloadData()
        list.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.bottom.equalTo(self.bottomLayoutGuide.snp.top)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath as IndexPath) as! InfoCellTableViewCell

       // imageView.sd_setImage(with: URL(string: "http://www.domain.com/path/to/image.jpg"), placeholderImage: UIImage(named: "placeholder.png"))

        let imageURL: URL! = Foundation.URL(string: self.list_playlists[indexPath.row].picture_medium)
        cellInfo.playlist_image.sd_setImage(with: imageURL)
        
        cellInfo.playlist_length.text = "\(list_playlists[indexPath.row].nb_tracks) pistes"
        if (list_playlists[indexPath.row].creation_date != "") {
            cellInfo.playlist_date.text = "crée le \(list_playlists[indexPath.row].creation_date)"
        }
        else {
            cellInfo.playlist_date.text = "date de création inconue"
        }
        cellInfo.playlist_title.text = list_playlists[indexPath.row].title
        cellInfo.playlist_user.text = list_playlists[indexPath.row].name

        return cellInfo
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list_playlists.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

